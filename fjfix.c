/* fjfix is a tool for handling FileJoin (FJSYS) archives.

  In addition to pack/unpack functionality fixes file index on existing
  archives to work with either WINE, Windows or POSIX string collation
  order, allowing poorly coded applications run in WINE.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <sys/stat.h>

#ifndef VERSION
#define VERSION "<devel>"
#endif

#ifdef _WIN32
#include <windows.h>
#define strcasecmp lstrcmpiA

#ifndef __WINE__
#include <dirent.h>
#define mkdir(n, p) mkdir(n)
#endif

#endif

#define FJHDR_SZ 0x54

struct fjhdr
{
	char magic[8];
	unsigned data_off;
	unsigned names_size;
	unsigned file_num;
	unsigned char padding[FJHDR_SZ - 8 - sizeof(unsigned)*3];
};

struct fjtabent
{
	unsigned name_off;
	unsigned data_size;
	unsigned data_off;
	unsigned padding;
};

struct fjfile
{
	char fjname[PATH_MAX];
	FILE* file;

	unsigned options;

	struct fjhdr* hdr;
	struct fjtabent* tab;
	unsigned nent, tabsz;

	char* names;
};

struct fjentry
{
	struct fjfile* fjfile;
	unsigned tabind;
	unsigned seekpos;
};

enum {FJFILE_VERBOSE=0x1};

typedef struct fjfile* fjfile_t;
typedef struct fjentry* fjentry_t;

static void badmsg(const char* name, const char* msg)
{
	fprintf(stderr, "Bad FJ archive '%s': %s\n", name, msg);
}

fjfile_t fjfile_open(const char* name)
{
	fjfile_t s = malloc(sizeof(*s));

	if (strlen(name) > sizeof(s->fjname))
		return 0;
	strcpy(s->fjname, name);

	s->hdr = 0;
	s->tab = 0;
	s->nent = s->tabsz = 0;
	s->names = 0;
	s->options = 0;

	s->file = fopen(name, "r+b");
	if (!s->file) {
		fprintf(stderr, "Can't open file '%s': %s\n", name, strerror(errno));
		goto err;
	}

	s->hdr = malloc(sizeof(*s->hdr));
	if (fread(s->hdr, sizeof(*s->hdr), 1, s->file) < 1) {
		badmsg(name, "header too small");
		goto err;
	}

	if (strncmp(s->hdr->magic, "FJSYS", 6)) {
		badmsg(name, "bad magic");
		goto err;
	}

	/* Limits arbitrary */
	if (s->hdr->file_num > 65535 || s->hdr->names_size > 16*65536 - 1 || s->hdr->data_off > (1 << 20)) {
		badmsg(name, "header sanity check failed");
		goto err;
	}

	s->nent = s->hdr->file_num;
	s->tabsz = s->nent;
	s->tab = malloc(s->nent*sizeof(*s->tab));
	if (fread(s->tab, sizeof(*s->tab), s->nent, s->file) < s->nent) {
		badmsg(name, "incomplete entry table");
		goto err;
	}

	int i;
	for (i = 0; i < s->nent; i++) {
		if (s->tab[i].name_off >= s->hdr->names_size) {
			badmsg(name, "entry name offset outside name section");
			goto err;
		}
	}

	s->names = malloc(s->hdr->names_size);
	if (fread(s->names, s->hdr->names_size, 1, s->file) < 1) {
		badmsg(name, "incomplete name section");
		goto err;
	}

	return s;

err:
	if (s->file)
		fclose(s->file);
	if (s->hdr)
		free(s->hdr);
	if (s->tab)
		free(s->tab);
	if (s->names)
		free(s->names);
	free(s);
	return 0;
}

const char* fjfile_name(fjfile_t s)
{
	return s->fjname;
}

unsigned fjfile_set_options(fjfile_t s, unsigned flags)
{
	return s->options |= flags;
}

unsigned fjfile_reset_options(fjfile_t s, unsigned flags)
{
	return s->options &= ~flags;
}

fjentry_t fjfile_first_entry(fjfile_t fjfile)
{
	fjentry_t s = malloc(sizeof(*s));

	s->fjfile = fjfile;
	s->tabind = 0;
	s->seekpos = 0;
	return s;
}

fjentry_t fjentry_next(fjentry_t s)
{
	s->seekpos = 0;

	if (++s->tabind < s->fjfile->nent)
		return s;
	free(s);
	return 0;
}

const char* fjentry_name(fjentry_t s)
{
	return s->fjfile->names + s->fjfile->tab[s->tabind].name_off;
}

unsigned fjentry_size(fjentry_t s)
{
	return s->fjfile->tab[s->tabind].data_size;
}

int fjentry_seek(fjentry_t s, unsigned seekpos)
{
	if (seekpos > s->fjfile->tab[s->tabind].data_size)
		return -1;

	s->seekpos = seekpos;
	return 0;
}

unsigned fjentry_read(fjentry_t s, void* buf, unsigned size)
{
	struct fjtabent* te = &s->fjfile->tab[s->tabind];

	if (size > te->data_size - s->seekpos)
		size = te->data_size - s->seekpos;

	if (!size)
		return 0;

	if (fseek(s->fjfile->file, te->data_off + s->seekpos, SEEK_SET) < 0)
		goto err;

	if (fread(buf, size, 1, s->fjfile->file) != 1) {
		if (feof(s->fjfile->file)) {
			fprintf(stderr, "Reading entry '%s': unexpected EOF at 0x%08X + %d\n",
				s->fjfile->names + te->name_off, s->seekpos, size);
			return 0;
		} else
			goto err;
	}

	s->seekpos += size;

	return size;
err:
	fprintf(stderr, "Reading entry '%s' failed: %s\n",
		s->fjfile->names + te->name_off, strerror(errno));
	return 0;
}

static int fjfile_tab_cmpswap(fjfile_t s, unsigned i1, unsigned i2)
{
	char* f1 = s->names + s->tab[i1].name_off;
	char* f2 = s->names + s->tab[i2].name_off;

	if (strcasecmp(f1, f2) > 0) {
		if (s->options & FJFILE_VERBOSE) {
			printf("Sort: 1: '%s' > 2: '%s', swapping\n", f1, f2);
			fflush(stdout);
		}

		struct fjtabent t = s->tab[i1];
		s->tab[i1] = s->tab[i2];
		s->tab[i2] = t;

		return 1;
	} else
		return 0;
}

int fjfile_sort_index(fjfile_t s)
{
	int passes, swaps;

	for (swaps = 0, passes = 0; passes < s->nent; passes++) {
		int i, sw;

		if (s->options & FJFILE_VERBOSE) {
			printf("Sort: DOWN\n");
			fflush(stdout);
		}

		for (sw = 0, i = 0; i < s->nent - 1; i++)
			sw += fjfile_tab_cmpswap(s, i, i + 1);
		swaps += sw;
		if (!sw)
			return swaps;

		if (s->options & FJFILE_VERBOSE) {
			printf("Sort: UP\n");
			fflush(stdout);
		}

		for (sw = 0, i = s->nent - 1; i > 0; i--)
			sw += fjfile_tab_cmpswap(s, i - 1, i);
		swaps += sw;
		if (!sw) {
			return swaps;
		}
	}

	fprintf(stderr, "Sort: pairwise comparison not transitive, aborting\n");

	return -1;
}

int fjfile_write_header(fjfile_t s)
{
	if (fseek(s->file, 0, SEEK_SET) < 0)
		goto err;
	if (fwrite(s->hdr, sizeof(*s->hdr), 1, s->file) != 1)
		goto err;
	if (fwrite(s->tab, sizeof(*s->tab), s->hdr->file_num, s->file) != s->hdr->file_num)
		goto err;
	if (fwrite(s->names, s->hdr->names_size, 1, s->file) != 1)
		goto err;

	return 0;
err:
	fprintf(stderr, "Writing header tables failed: %s\n", strerror(errno));
	return -1;
}

#define COPYBUF_SZ 4096

#define BACKUP_SUFFIX ".backup"

static int mkbackup(char* fname, int progress)
{
	char backfname[PATH_MAX];

	if (strlen(fname) > sizeof(backfname) - sizeof(BACKUP_SUFFIX))
		return -1;
	strcpy(backfname, fname);
	strcat(backfname, BACKUP_SUFFIX);

	FILE* backf = fopen(backfname, "rb");
	FILE* f = 0;
	if (!backf) {
		f = fopen(fname, "rb");
		if (!f)
			goto err;

		if (progress) {
			printf("Creating backup: '%s'\n", backfname);
			fflush(stdout);
		}

		backf = fopen(backfname, "wb");
		if (!backf)
			goto err;

		size_t n, sz = 0;
		char buf[COPYBUF_SZ];
		while ((n = fread(buf, 1, sizeof(buf), f)) > 0) {
			if (fwrite(buf, n, 1, backf) != 1)
				goto err;
			sz += n;
		}
		if (progress) {
			printf("'%s': %d kb copied\n", backfname, sz >> 10);
			fflush(stdout);
		}

		fclose(f);
	}

	fclose(backf);
	return 0;

err:
	if (backf)
		fclose(backf);
	if (f)
		fclose(f);
	fprintf(stderr, "Creating backup '%s' -> '%s' failed: %s\n",
		fname, backfname, strerror(errno));
	return -1;
}

static void list(fjfile_t fjf)
{
	fjentry_t ent;

	for (ent = fjfile_first_entry(fjf); ent; ent = fjentry_next(ent)) {
		if (fjf->options & FJFILE_VERBOSE) {
			printf("'%s' : name_off: %d, data_size: %d, data_off: 0x%X\n",
			       fjentry_name(ent), fjf->tab[ent->tabind].name_off,
			       fjentry_size(ent), fjf->tab[ent->tabind].data_off);
		} else {
			printf("%s %d\n", fjentry_name(ent), fjentry_size(ent));
		}
	}

	fflush(stdout);
}

#define EXTRACT_SUFFIX ".d"

static char* format_destdir(const char* fjname, const char* destdir)
{
	char* ddir;

	if (!(destdir && *destdir)) {
		const char* fname = strrchr(fjname, '/');
		if (!fname)
			fname = fjname;
		else
			fname++;

		ddir = malloc(strlen(fname) + sizeof(EXTRACT_SUFFIX));
		sprintf(ddir, "%s" EXTRACT_SUFFIX, fname);
	} else {
		size_t pl = strlen(destdir);
		ddir = malloc(pl + 1);
		strcpy(ddir, destdir);
		while (--pl && ddir[pl] == '/')
			ddir[pl] = '\0';
	}

	return ddir;
}

static int mkextractdir(const char* ddir)
{
	struct stat dstat;
	if (stat(ddir, &dstat) < 0) {
		if (mkdir(ddir, 0777) < 0) {
			fprintf(stderr, "Can't create destination directory '%s': %s\n",
				ddir, strerror(errno));
			return -1;
		}
	} else {
		if (!S_ISDIR(dstat.st_mode)) {
			fprintf(stderr, "Not a directory: '%s'\n", ddir);
			return -1;
		}
	}

	return 0;
}

static int entry_extract(fjentry_t s, const char* fname)
{
	FILE* f = fopen(fname, "wb");
	if (!f)
		goto err;

	printf("Extracting '%s' to '%s', size %d bytes\n",
	       fjentry_name(s), fname, fjentry_size(s));
	fflush(stdout);

	fjentry_seek(s, 0);

	size_t n, sz = 0;
	char buf[COPYBUF_SZ];
	while ((n = fjentry_read(s, buf, sizeof(buf))) > 0) {
		if (fwrite(buf, n, 1, f) != 1)
			goto err;
		sz += n;
	}

	fclose(f);

	if (sz != fjentry_size(s)) {
		fprintf(stderr, "Truncated file '%s': %d of %d bytes written\n",
			fjentry_name(s), sz, fjentry_size(s));
		return -1;
	}

	return 0;
err:
	if (f)
		fclose(f);
	fprintf(stderr, "Can't extract to file '%s': %s\n", fname, strerror(errno));
	return -1;
}

#define MGDHDR_SZ 0x60
#define MGD_MAGIC "MGD "

struct mgdhdr
{
	char magic[4];
	char padding[MGDHDR_SZ - 8];
	unsigned png_size;
};

#define PNG_MAGIC "\x89PNG\r\n\x1A\n"

static int filter_mgd_test(fjentry_t s)
{
	struct mgdhdr hdr;
	unsigned sz;

	sz = fjentry_read(s, &hdr, sizeof(hdr));
	if (sz != sizeof(hdr))
		return 0;

	if (strncmp(hdr.magic, MGD_MAGIC, sizeof(hdr.magic)))
		return 0;

	char magic[sizeof(PNG_MAGIC) - 1];
	sz = fjentry_read(s, magic, sizeof(magic));
	if (strncmp(magic, PNG_MAGIC, sizeof(magic)))
		return 0;

	return 1;
}

static int entry_mgd_extract(fjentry_t s, const char* fname)
{
	FILE* f = fopen(fname, "wb");
	if (!f)
		goto err;

	struct mgdhdr hdr;
	fjentry_seek(s, 0);
	fjentry_read(s, &hdr, sizeof(hdr));

	printf("PNG extracting '%s' to '%s', size %d bytes\n",
	       fjentry_name(s), fname, hdr.png_size);
	fflush(stdout);

	size_t n, sz = 0;
	char buf[COPYBUF_SZ];
	while ((n = fjentry_read(s, buf, sizeof(buf))) > 0) {
		if (sz + n > hdr.png_size)
			n = hdr.png_size - sz;
		if (!n)
			break;
		if (fwrite(buf, n, 1, f) != 1)
			goto err;
		sz += n;
	}

	fclose(f);

	if (sz != hdr.png_size) {
		fprintf(stderr, "PNG extract truncated file '%s': %d of %d bytes written\n",
			fjentry_name(s), sz, fjentry_size(s));
		return -1;
	}

	return 0;
err:
	if (f)
		fclose(f);
	fprintf(stderr, "Can't extract PNG to file '%s': %s\n", fname, strerror(errno));
	return -1;
}

enum {EX_FILTER_MGD = 0x1};

static int extract(fjfile_t fjf, const char* destdir, unsigned filters)
{
	char fname[PATH_MAX];
	char* ddir = format_destdir(fjfile_name(fjf), destdir);

	if (mkextractdir(ddir) < 0) {
		free(ddir);
		return -1;
	}

	fjentry_t ent;
	for (ent = fjfile_first_entry(fjf); ent; ent = fjentry_next(ent)) {
		if ((filters & EX_FILTER_MGD) && filter_mgd_test(ent)) {
			int l = sprintf(fname, "%s/%s.png", ddir, fjentry_name(ent));
			char* ext = fname + l - (sizeof(".mgd.png") - 1);
			if (ext > fname && !strcasecmp(ext, ".mgd.png"))
				strcpy(ext, ".png");
			entry_mgd_extract(ent, fname);
		} else {
			sprintf(fname, "%s/%s", ddir, fjentry_name(ent));
			entry_extract(ent, fname);
		}
	}

	free(ddir);
	return 0;
}

static const char* usage =
	"Usage: fjfix [-fhltv] FILE [DESTDIR]\n"
	"Fix entry index of, extract or create FJSYS (FileJoin) archive FILE.\n"
	"Options:\n"
	"    -b - NO backup, otherwise creates FILE.backup before modification.\n"
	"    -f - fix entry index sort order to match system string collation order.\n"
	"         Fixes 'file not found' errors for broken collation order dependent software in WINE.\n" 
	"         Uses WINE (unicode.org) collation order when run in WINE,\n"
	"         Windows (yet another non-standard) when run in Windows.\n"
	"         Native unix version uses POSIX (ASCII code) order, incompatible with both of above.\n"
	"    -h - this help.\n"
	"    -l - archive entry list with byte sizes.\n"
	"    -t - test entry index, do not fix it. Enabled by default.\n"
	"    -x - extract entries as files to DESTDIR or FILE.d if DESDIR not specified.\n"
	"    -v - verbose. Print additional data which might be useful for problem analysis and reporting.\n"
	"    -G - additionally extract PNG files embedded in MGD container entries. Must be used with -x.\n"
	"Version " VERSION " built " __DATE__ "\n";

enum {
	OPT_TEST = 0x1, OPT_LIST = 0x2, OPT_FIX = 0x4, OPT_BACKUP = 0x8,
	OPT_EXTRACT = 0x10,
	OPT_VERBOSE = 0x1000
};

int main(int argc, char* argv[0])
{
	int opt;
	unsigned options = OPT_TEST | OPT_BACKUP;
	unsigned ext_filters = 0;
	int needfix = 0;
	char* fname;

	while ((opt = getopt(argc, argv, "Gblvtfhx")) != -1) {
		switch (opt) {
		case 'G':
			ext_filters |= EX_FILTER_MGD;
			break;
		case 'b':
			options &= ~OPT_BACKUP;
			break;
		case 'f':
			options |= OPT_FIX;
			break;
		case 'l':
			options |= OPT_LIST;
			break;
		case 't':
			options |= OPT_TEST;
			break;
		case 'x':
			options |= OPT_EXTRACT;
			break;
		case 'v':
			options |= OPT_VERBOSE;
			break;
		case 'h':
			fprintf(stdout, "%s", usage);
			return 0;
		case '?':
		default:
			fprintf(stderr, "Try -h for options info.\n");
			return 1;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "FILE argument required\n");
		fprintf(stderr, "Try -h for usage info.\n");
		return 1;
	}

	fname = argv[optind];

	fjfile_t fjf = fjfile_open(fname);
	if (!fjf) {
		fprintf(stderr, "Reading '%s' failed\n", fname);
		return 2;
	}

	if (options & OPT_VERBOSE) {
		fjfile_set_options(fjf,  FJFILE_VERBOSE);
		printf("HEADER: file_num: %d, names_size: %d, data_off: 0x%X\n",
		       fjf->hdr->file_num, fjf->hdr->names_size, fjf->hdr->data_off);
		fflush(stdout);
	}

	if (options & OPT_LIST) {
		list(fjf);
	}

	if (options & OPT_EXTRACT) {
		char* destdir = 0;

		if (optind + 1 < argc)
			destdir = argv[optind + 1];

		extract(fjf, destdir, ext_filters);
	}

	if (options & (OPT_TEST | OPT_FIX)) {
		int r = fjfile_sort_index(fjf);
		if (r < 0)
			return 3;

		needfix = r > 0;
	}

	if (needfix) {
		fprintf(stderr, "File index needs rebuilding\n");
	}

	if (needfix && (options & OPT_FIX)) {
		if (options & OPT_BACKUP) {
			if (mkbackup(fname, 1) < 0)
				return 4;
		}
		printf("Updating index\n");
		fflush(stdout);

		if (fjfile_write_header(fjf) < 0)
			return 5;
	}

	return 0;
}
