VERSION='"0.3"'
DEFS=-DVERSION=${VERSION}

MINGWGCC=i686-pc-mingw32-gcc

CFLAGS:=-O2 -Wall ${DEFS} ${CFLAGS}

all: fjfix fjfixw.exe fjfix.exe

fjfix: fjfix.c
	gcc -o $@ ${CFLAGS} $<

fjfixw.exe: fjfix.c
	winegcc -o $@ ${CFLAGS} $<

fjfix.exe: fjfix.c
	${MINGWGCC} -o $@ ${CFLAGS} $<

clean:
	rm -f fjfix fjfix.exe fjfixw.exe.so fjfixw.exe
